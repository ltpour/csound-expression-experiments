--
-- published as "How Bright Is the Ocean" by Caves of Ogygia
-- on Literally Nervous Records (NERVOUS-009) in 2016
--

module Main where

import Csound.Base

main =
  return 0 -- do nothing. output function is at the end of the file.

duration = 1986

-- instruments

instr (amp, cps) = mul (0.65 * sig amp * fades 0.05 0.2) $  
    fmap (alp1 (3200 * leg 0.4 0.65 0.85 1.3) 0.1) $ 
    rndSaw (sig cps) + rndTri (3.01 * sig cps) + rndSaw (5.01 * sig cps) + rndTri (7.01 * sig cps)

-- effects

fxs = [FxSpec 0.45 (return . magicCave2)]

fxs2 = [FxSpec 0.45 (return . stChorus2 1 1 1 1)]

-- patches

p = Patch (fmap fromMono . instr) fxs

p2 = Patch (fmap fromMono . instr) fxs2

-- notes

ns1 = fmap temp [(0.5, 330), (0.6, 270), (0.5, 410), (0.65, 580), (0.75, 440), (1, 550), (0.75, 720)]

ns2 = fmap temp [(0.25, 510), (0.45, 630), (0.65, 660), (0.55, 580), (0.45, 490), (0.85, 425), (0.75, 720)]

notes1 = str 14.5 $ har [mel [rest 2, mel ns1, har [mel ns1, har [mel ns1, mel ns1]], rest 2], mel [rest 1, mel [har [mel ns1, mel ns1], mel ns1], mel ns1, har [mel ns1, mel ns1], rest 1]]

notes2 = str 17.5 $ har [mel [mel ns2, har [mel ns2, mel ns2], rest 4], mel [rest 3, mel ns2, mel ns2, har [mel ns2, mel ns2], rest 1]]

-- tracks

melodyTrack1 = mul 0.35 $ mixLoop $ atSco p notes1

melodyTrack2 = mul 0.35 $ mixLoop $ atSco p notes2

grainTrack1 =
  syncgrainSnd def 0.0015 (2.45) 0.35 "melodyTrack1.wav"

grainTrack2 =
  syncgrainSnd def 0.005 (1.65) 0.25 "melodyTrack1.wav"

grainTrack3 =
  syncgrainSnd def 0.015 (1.35) 0.45 "melodyTrack1.wav"

-- functions for playing and writing tracks to disk

playMelodyTrack1 =
  dac $ melodyTrack1

writeMelodyTrack1 =
  writeSnd "melodyTrack1.wav" $ melodyTrack1

playMelodyTrack2 =
  dac $ melodyTrack2

writeMelodyTrack2 =
  writeSnd "melodyTrack2.wav" $ melodyTrack2

playGrainTrack1 =
  do
    writeMelodyTrack1
    dac $ setDur duration $ grainTrack1

writeGrainTrack1 =
  do
    writeMelodyTrack1
    writeSnd "grainTrack1.wav" $ setDur duration $ grainTrack1
  
playGrainTrack2 =
  do
    writeMelodyTrack1
    dac $ setDur duration $ grainTrack2

writeGrainTrack2 =
  do
    writeMelodyTrack1
    writeSnd "grainTrack2.wav" $ setDur duration $ grainTrack2
  
playGrainTrack3 =
  do
    writeMelodyTrack1
    dac $ setDur duration $ grainTrack3

writeGrainTrack3 =
  do
    writeMelodyTrack1
    writeSnd "grainTrack3.wav" $ setDur duration $ grainTrack3
  
playMix =
  do
    writeMelodyTrack1
    dac $ setDur duration $ melodyTrack1 + grainTrack1 + grainTrack2 + grainTrack2

writeMix = -- the complete piece. in the published version a fade out was added at the end.
  do
    writeMelodyTrack1
    writeSnd "mix.wav" $ setDur duration $ melodyTrack1 + grainTrack1 + grainTrack2 + grainTrack2
