--
-- published as Eismagie by Gegen das Psi
-- on Literally Nervous Records (NERVOUS-010) in 2016
--

module Main where

import Csound.Base

main =
  return 0 -- do nothing. output function is at the end of the file.

duration = 1889

-- instruments

instr1 (amp, cps) = mul (0.65 * sig amp * fades 0.5 0.02) $  
    fmap (alp1 (4200 * leg 1 3 0.5 3) 0.1) $ 
    rndOsc (sig cps) + rndOsc (1.75 * sig cps) + rndOsc (3.75 * sig cps) + rndOsc (5.75 * sig cps) + rndOsc (7.75 * sig cps)

instr2 (amp, cps) = mul (0.65 * sig amp * fades 0.65 0.05) $  
    fmap (alp1 (4000 * leg 1 3 0.5 3) 0.1) $ 
    rndSaw (sig cps) + rndOsc (3.75 * sig cps) + rndSaw (4.25 * sig cps) + rndOsc (5.05 * sig cps) + rndSaw (5.55 * sig cps)

instr3 (amp, cps) = mul (0.65 * sig amp * fades 0.75 0.1) $  
    fmap (alp1 (3900 * leg 1 3 0.5 3) 0.1) $ 
    rndOsc (sig cps) + rndTri (4.75 * sig cps) + rndOsc (7.25 * sig cps) + rndTri (9.05 * sig cps) + rndOsc (11.55 * sig cps)

-- effects

fxs1 = [FxSpec 0.65 (return . stChorus2 1 0.235 0.3 0.75 . magicCave2)]

fxs2 = [FxSpec 0.65 (return . fxFlanger2 1.35 0.25 0.45 0.6 0.2 . stChorus2 2 1.35 3 0.75 . magicCave2)]

fxs3= [FxSpec 0.35 (return . fxDistort2 0.015 0.45 0.75 . largeHall2)]

-- patches

p1 = Patch (fmap fromMono . instr1) fxs1

p2 = Patch (fmap fromMono . instr2) fxs2

p3 = Patch (fmap fromMono . instr3) fxs3

-- notes

ns1 = fmap temp [(0.5, 270), (0.6, 340), (0.5, 320), (0.65, 480), (0.75, 520), (1, 470), (0.75, 490), (0.65, 590), (0.7, 645), (0.75, 635), (0.8, 695), (0.75, 720)]

ns2 = fmap temp [(0.35, 510), (0.45, 470), (0.65, 520), (0.55, 490), (0.65, 540), (0.75, 585), (0.7, 565), (0.65, 635), (0.7, 665), (0.75, 705), (0.65, 675), (0.6, 645)]

ns3 = fmap temp [(0.35, 710), (0.45, 570), (0.65, 620), (0.55, 890), (0.65, 640), (0.75, 785), (0.7, 965), (0.65, 835), (0.7, 1065), (0.75, 905), (0.65, 875), (0.6, 745)]

notes1 = str 12.5 $ har [mel [har [mel ns1, mel ns2], mel ns3, har [mel ns2, mel ns1], rest 1, mel ns2, har [mel ns3, mel ns2], har [mel ns1, har [mel ns2, mel ns3]], rest 3], mel [rest 1, mel [har [mel ns2, mel ns1], mel ns2], mel ns3, har [mel ns2, mel ns1], rest 1]]

notes2 = str 17.5 $ har [mel [har [mel ns1, mel ns2, mel ns3], mel ns1, har [har [mel ns2, mel ns3], mel ns1, mel ns2], rest 3], mel [har [mel ns3, mel ns1], rest 3, mel ns2, mel ns3, har [mel ns1, mel ns2], rest 2]]

notes3 = str 15.5 $ har [mel [har [har [har [mel ns1, mel ns2], mel ns3, mel ns1], mel ns2, mel ns3, mel ns1], mel ns2, rest 3], mel [har [mel ns3, mel ns1], har [mel ns2, mel ns3], rest 2, rest 3, mel ns1, mel ns2]]

-- tracks

melodyTrack1 = mul 0.35 $ mixLoop $ atSco p1 notes1

melodyTrack2 = mul 0.35 $ mixLoop $ atSco p2 notes2

melodyTrack3 = mul 0.35 $ mixLoop $ atSco p3 notes3

grainTrack1 =
  mul 0.65 $ syncgrainSnd def 0.0015 (2.65) 0.45 "melodyTrack1.wav"

grainTrack2 =
  mul 0.65 $ syncgrainSnd def 0.0025 (3.35) 0.15 "melodyTrack2.wav"

grainTrack3 =
  mul 0.65 $ syncgrainSnd def 0.015 (1.55) 0.25 "melodyTrack3.wav"

-- functions for playing and writing tracks to disk

playMelodyTrack1 =
  dac $ melodyTrack1

writeMelodyTrack1 =
  writeSnd "melodyTrack1.wav" $ melodyTrack1

playMelodyTrack2 =
  dac $ melodyTrack2

writeMelodyTrack2 =
  writeSnd "melodyTrack2.wav" $ melodyTrack2

playMelodyTrack3 =
  dac $ melodyTrack3

writeMelodyTrack3 =
  writeSnd "melodyTrack3.wav" $ melodyTrack3

playGrainTrack1 =
  do
    writeMelodyTrack1
    dac $ setDur duration $ grainTrack1

writeGrainTrack1 =
  do
    writeMelodyTrack1
    writeSnd "grainTrack1.wav" $ setDur duration $ grainTrack1
  
playGrainTrack2 =
  do
    writeMelodyTrack2
    dac $ setDur duration $ grainTrack2

writeGrainTrack2 =
  do
    writeMelodyTrack2
    writeSnd "grainTrack2.wav" $ setDur duration $ grainTrack2
  
playGrainTrack3 =
  do
    writeMelodyTrack3
    dac $ setDur duration $ grainTrack3

writeGrainTrack3 =
  do
    writeMelodyTrack3
    writeSnd "grainTrack3.wav" $ setDur duration $ grainTrack3
  
playMix =
  do
    writeMelodyTrack1
    writeMelodyTrack2
    writeMelodyTrack3
    dac $ mul (linseg [0, 0.5, 1, duration - 8.5, 1, 8, 0]) . setDur duration $ 0.55 * melodyTrack1 + 0.55 * melodyTrack3 + grainTrack1 + grainTrack2 + grainTrack3

writeMix = -- the complete piece. in the published version a fade out was added at the end.
  do
    writeMelodyTrack1
    writeMelodyTrack2
    writeMelodyTrack3
    writeSnd "mix.wav" $ mul (linseg [0, 0.5, 1, duration - 8.5, 1, 8, 0]) . setDur duration $ 0.55 * melodyTrack1 + 0.55 * melodyTrack3 + grainTrack1 + grainTrack2 + grainTrack3
