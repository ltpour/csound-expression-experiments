--
-- published as Tapers by Meh-Hum
-- on Literally Nervous Records (NERVOUS-011) in 2016
--

module Main where

import Csound.Base

main =
  return 0 -- do nothing. output function is at the end of the file.

duration = 1881

-- instruments

instr1 (amp, cps) = mul (0.65 * sig amp * fades 0.5 0.02) $  
    fmap (alp1 (4200 * leg 1 3 0.5 3) 0.1) $ 
    rndOsc (sig cps) + rndOsc (1.45 * sig cps) + rndOsc (2.25 * sig cps) + rndOsc (3.35 * sig cps) + rndOsc (4.45 * sig cps) + rndOsc (5.15 * sig cps) + rndOsc (5.25 * sig cps) + rndOsc (6.65 * sig cps) + rndOsc (6.95 * sig cps) + rndOsc (8.25 * sig cps)

instr2 (amp, cps) = mul (0.65 * sig amp * fades 0.65 0.05) $  
    fmap (alp1 (4000 * leg 1 3 0.5 3) 0.1) $ 
    rndSaw (sig cps) + rndOsc (2.75 * sig cps) + rndSaw (3.15 * sig cps) + rndOsc (4.55 * sig cps) + rndSaw (5.65 * sig cps) + rndSaw (6.05 * sig cps) + rndOsc (7.75 * sig cps) + rndSaw (8.25 * sig cps) + rndOsc (9.05 * sig cps) + rndSaw (9.55 * sig cps)

instr3 (amp, cps) = mul (0.65 * sig amp * fades 0.75 0.1) $  
    fmap (alp1 (3900 * leg 1 3 0.5 3) 0.1) $ 
    rndOsc (sig cps) + rndTri (3.75 * sig cps) + rndOsc (4.25 * sig cps) + rndTri (5.05 * sig cps) + rndOsc (6.55 * sig cps) + rndOsc (7.05 * sig cps) + rndTri (7.75 * sig cps) + rndOsc (9.25 * sig cps) + rndTri (10.05 * sig cps) + rndOsc (11.25 * sig cps)

instr4 (amp, cps) = mul (0.65 * sig amp * fades 0.75 0.1) $  
    fmap (alp1 (3900 * leg 1 3 0.5 3) 0.1) $ 
    rndTri (sig cps) + rndTri (3.70 * sig cps) + rndTri (4.35 * sig cps) + rndSaw (5.00 * sig cps) + rndTri (6.50 * sig cps) + rndSaw (7.15 * sig cps) + rndOsc (7.85 * sig cps) + rndTri (9.35 * sig cps) + rndSaw (10.00 * sig cps) + rndTri (11.20 * sig cps)

-- effects

fxs1 = [FxSpec 0.35 (return . fxDistort2 0.015 0.45 0.75 . largeHall2)]

fxs2 = [FxSpec 0.65 (return . fxDistort2 0.025 0.35 0.45 . stChorus2 1 0.235 0.3 0.75 . magicCave2)]

fxs3 = [FxSpec 0.65 (return . stChorus2 2 1.35 3 0.75 . fxDistort2 0.075 0.45 0.65 . magicCave2)]

fxs4 = [FxSpec 0.55 (return . fxDistort2 0.035 0.25 0.45 . stChorus2 2 1.35 3 0.75 . largeHall2)]

-- patches

p1 = Patch (fmap fromMono . instr1) fxs1

p2 = Patch (fmap fromMono . instr2) fxs2

p3 = Patch (fmap fromMono . instr3) fxs3

p4 = Patch (fmap fromMono . instr4) fxs4

-- notes

ns1 = fmap temp [(0.5, 280), (0.6, 343), (0.5, 322), (0.65, 488), (0.75, 553), (1, 336), (0.75, 346), (0.65, 494), (0.7, 545), (0.75, 535), (0.8, 635), (0.75, 520)]

ns2 = fmap temp [(0.35, 540), (0.45, 370), (0.65, 520), (0.55, 490), (0.65, 560), (0.75, 585), (0.7, 565), (0.65, 615), (0.7, 645), (0.75, 705), (0.65, 685), (0.6, 675)]

ns3 = fmap temp [(0.35, 610), (0.45, 570), (0.65, 620), (0.55, 750), (0.65, 740), (0.75, 735), (0.7, 825), (0.65, 835), (0.7, 965), (0.75, 945), (0.65, 975), (0.6, 945)]

ns4 = fmap temp [(0.55, 710), (0.45, 770), (0.65, 720), (0.55, 850), (0.65, 840), (0.75, 835), (0.7, 825), (0.65, 835), (0.7, 765), (0.75, 745), (0.65, 775), (0.6, 745)]

notes1 = str 14.5 $ har [mel [har [mel ns2, mel ns3, mel ns2], mel ns1, har [har [mel ns2, mel ns1], mel ns2, mel ns3], rest 2], mel [har [mel ns2, mel ns1], rest 1, mel ns3, mel ns2, har [mel ns1, mel ns2], rest 1], mel ns2, har [mel ns1, mel ns3], har [mel ns2, mel ns3], har [mel ns4, mel ns2]]

notes2 = str 15.5 $ har [mel [har [har [har [mel ns1, mel ns4], mel ns3, mel ns1], mel ns2, mel ns3, mel ns1], mel ns2, rest 3], mel [har [mel ns3, mel ns1], har [mel ns2, mel ns3], rest 2, rest 3, mel ns1, mel ns2], mel ns2, mel ns1, mel ns3]

notes3 = str 13.5 $ har [mel [har [mel ns1, mel ns2], mel ns4, har [mel ns2, mel ns1], rest 1, mel ns2, har [mel ns3, mel ns2], har [mel ns1, har [mel ns2, mel ns3]], rest 3], mel [rest 1, mel [har [mel ns2, mel ns1], mel ns2], mel ns3, har [mel ns2, mel ns1], rest 1]]

notes4 = str 15.5 $ har [har [mel ns1, mel ns4], har [mel ns2, mel ns3], mel [har [har [har [mel ns1, mel ns4], mel ns3, mel ns1], mel ns2, mel ns3, mel ns1], mel ns2, rest 3], mel [har [mel ns3, mel ns1], har [mel ns4, mel ns3], rest 2, rest 3, mel ns1, mel ns2], mel ns4, mel ns4, mel ns3]

-- tracks

melodyTrack1 = mul 0.40 $ mixLoop $ atSco p1 notes1

melodyTrack2 = mul 0.45 $ mixLoop $ atSco p2 notes2

melodyTrack3 = mul 0.40 $ mixLoop $ atSco p3 notes3

melodyTrack4 = mul 0.55 $ mixLoop $ atSco p4 notes4

grainTrack1 =
  mul 0.55 $ syncgrainSnd def 0.0015 (2.65) 0.45 "melodyTrack1.wav"

grainTrack2 =
  mul 0.65 $ syncgrainSnd def 0.0025 (3.35) 0.15 "melodyTrack2.wav"

grainTrack3 =
  mul 0.45 $ syncgrainSnd def 0.015 (1.55) 0.25 "melodyTrack3.wav"

grainTrack4 =
  mul 0.55 $ syncgrainSnd def 0.0035 (1.85) 0.35 "melodyTrack4.wav"

-- functions for playing and writing tracks to disk

playMelodyTrack1 =
  dac $ melodyTrack1

writeMelodyTrack1 =
  writeSnd "melodyTrack1.wav" $ melodyTrack1

playMelodyTrack2 =
  dac $ melodyTrack2

writeMelodyTrack2 =
  writeSnd "melodyTrack2.wav" $ melodyTrack2

playMelodyTrack3 =
  dac $ melodyTrack3

writeMelodyTrack3 =
  writeSnd "melodyTrack3.wav" $ melodyTrack3

playMelodyTrack4 =
  dac $ melodyTrack4

writeMelodyTrack4 =
  writeSnd "melodyTrack4.wav" $ melodyTrack4

playGrainTrack1 =
  do
    writeMelodyTrack1
    dac $ setDur duration $ grainTrack1

writeGrainTrack1 =
  do
    writeMelodyTrack1
    writeSnd "grainTrack1.wav" $ setDur duration $ grainTrack1
  
playGrainTrack2 =
  do
    writeMelodyTrack2
    dac $ setDur duration $ grainTrack2

writeGrainTrack2 =
  do
    writeMelodyTrack2
    writeSnd "grainTrack2.wav" $ setDur duration $ grainTrack2
  
playGrainTrack3 =
  do
    writeMelodyTrack3
    dac $ setDur duration $ grainTrack3

writeGrainTrack3 =
  do
    writeMelodyTrack3
    writeSnd "grainTrack3.wav" $ setDur duration $ grainTrack3
  
playGrainTrack4 =
  do
    writeMelodyTrack4
    dac $ setDur duration $ grainTrack4

writeGrainTrack4 =
  do
    writeMelodyTrack4
    writeSnd "grainTrack4.wav" $ setDur duration $ grainTrack4
  
playMix =
  do
    writeMelodyTrack1
    writeMelodyTrack2
    writeMelodyTrack3
    writeMelodyTrack4
    dac $ mul (linseg [0, 0.5, 1, duration - 8.5, 1, 8, 0]) . setDur duration $ 0.45 * melodyTrack1 + 0.45 * melodyTrack3 + 0.35 * melodyTrack4 + grainTrack1 + grainTrack2 + grainTrack3 + grainTrack4

writeMix =
  do
    writeMelodyTrack1
    writeMelodyTrack2
    writeMelodyTrack3
    writeMelodyTrack4
    writeSnd "mix.wav" $ mul (linseg [0, 0.5, 1, duration - 8.5, 1, 8, 0]) . setDur duration $ 0.45 * melodyTrack1 + 0.45 * melodyTrack3 + 0.35 * melodyTrack4 +     grainTrack1 + grainTrack2 + grainTrack3 + grainTrack4
